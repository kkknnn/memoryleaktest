#include "memoryleakchecker.h"
#include <algorithm>

void MemoryLeakFinder::MemoryLeakCheck()
{


    if(AllocatedMemorySum==DeallocatedMemorySum)
    {
    std::cout << "[PASSED] Allocated/Deallocated memory sum correct. Alloc: " << AllocatedMemorySum << "Delloc: " << DeallocatedMemorySum << '\n';
    }
    else
    {
       std::cout << "[FAILED] Allocated/Deallocated memory sum incorrect. Alloc: " << AllocatedMemorySum << "Delloc: " << DeallocatedMemorySum << '\n';
    }

    if (NewCounter==DeleteCounter)
    {
        std::cout << "[PASSED] New / Delate call counter correct. Both called: " << NewCounter << " times" << '\n';
    }
    else
    {
        std::cout << "[FAILED] New / Delete call counter incorrect. Called new: " << NewCounter << " times" << " Called delete: " << DeleteCounter << " times" << '\n';
    }

    bool noCritError=true;
     for (auto iterator=ErrLogs.begin(); iterator!=ErrLogs.end();iterator++ )
     {
         if (*iterator!="")
             noCritError=false;
         break;
     }



    if (noCritError)
    {
        std::cout << "[PASSED] No critical error logs registered" << '\n';
    }
    else {
        std::cout << "[FAILED] Critical error logs: " << '\n';
        for (auto iterator=ErrLogs.begin(); iterator!=ErrLogs.end();iterator++ )
        {
            if(*iterator!="")
             std::cout << '\t' << *iterator << '\n';
        }
        }
    if (MemoTypes.empty())
    {
        std::cout << "[PASSED] All memory correctly freed" << '\n';
    }
    else
    {

        std::cout << "[FAILED] Not deallocated memory: " << '\n';
        for (auto iterator=MemoTypes.begin();iterator!=MemoTypes.end();iterator++)
        {
            if(iterator->first!="")
            {
            std::cout << '\t' << "Memory adress " << iterator->first << " variable size: " << iterator->second << '\n';

            }
        }
        std:: cout <<'\t'<< "------------------------------"<< '\n';

        for (auto iterator=MemoNewOrder.begin();iterator!=MemoNewOrder.end();iterator++)
        {
            if(iterator->first!="")
            {
            std::cout << '\t'<< "Memory adress " << iterator->first << " New operator call number " << iterator->second << '\n';

            }
        }
         std::cout << "[INFO] Call number may be affected by hidden allocation e.g. std::containers allocation " << '\n';
    }

}
void MemoryLeakFinder::AddAllocatedMemory(std::size_t MemorySize)
{
    AllocatedMemorySum+=MemorySize;
}

void MemoryLeakFinder::AddDeallocatedMemory(std::size_t MemorySize)
{
    DeallocatedMemorySum+=MemorySize;
}

void MemoryLeakFinder::AddOperatorNew()
{
    NewCounter+=1;
}

void MemoryLeakFinder::AddOperatorDelete()
{
    DeleteCounter+=1;
}

void MemoryLeakFinder::RegisterMemoryAllocated(std::string MemoAdress, std::size_t TypeName)
{

    for(auto iterator=MemoTypes.begin();iterator!=MemoTypes.end();iterator++)
    {
        if (iterator->first==MemoAdress)
        {
            std::string temp = "Allocation error: Tried to allocate previously allocated memory " + MemoAdress + "size: "+ std::to_string(TypeName);
           LogCriticalError(temp);

        }

    }
    for(auto iterator=MemoTypes.begin();iterator!=MemoTypes.end();iterator++)
    {
        if (iterator->first=="")
        {
            auto temp=
            *iterator = std::make_pair(MemoAdress,TypeName);
            break;
        }

    }

    for(auto iter=MemoNewOrder.begin();iter!=MemoNewOrder.end();iter++)
    {
        if (iter->first=="")
        {

            *iter = std::make_pair(MemoAdress, NewCounter);
            break;

        }

    }


}

void MemoryLeakFinder::RegisterMemoryDealocated(std::string MemoAdress)
{
    if(!MemoTypes.empty())
    {
        auto pointer = std::find_if(MemoTypes.begin(),MemoTypes.end(),[MemoAdress](auto elem){return MemoAdress==elem.first;});
        if(pointer==MemoTypes.end())
        {
            std::string temp = "Wrong delete use: probably deleting variable not allocated on heap  " + MemoAdress + '\n';
            LogCriticalError(temp);
        }
        else
        {
            this->AddDeallocatedMemory(pointer->second);
            *pointer={};
            auto pointer2 = std::find_if(MemoNewOrder.begin(),MemoNewOrder.end(),[MemoAdress](auto elem){return MemoAdress==elem.first;});
            *pointer2={};
        }


    }
    else
    {
        std::string temp = "Wrong delete use: Tried to release memory before allocated some" + MemoAdress + '\n';
        LogCriticalError(temp);
    }
}

void MemoryLeakFinder::CatchExeptions(std::exception &error)
{
    LogCriticalError(error.what());
}

int MemoryLeakFinder::GetOperatorNewCounter()
{
    return NewCounter;
}

void MemoryLeakFinder::LogCriticalError(std::string ErrorDescription)
{
    auto iter = std::find(ErrLogs.begin(),ErrLogs.end(),"");
        *iter= ErrorDescription;
}
