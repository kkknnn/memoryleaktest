#ifndef MEMORYLEAKCHECKER_H
#define MEMORYLEAKCHECKER_H
#include <iostream>
#include <exception>
#include <map>
#include <vector>


class MemoryLeakFinder
{
public:
#define ArrSize 30                             // maksymalna ilosc wywolan operatora new bez zwalniania
#define ErrLogsSize 10
    ~MemoryLeakFinder()=default;
    void MemoryLeakCheck();                           // funckja drukujaca wyniki
    void AddAllocatedMemory(std::size_t MemorySize);
    void AddDeallocatedMemory(std::size_t MemorySize);
    void AddOperatorNew();
    void AddOperatorDelete();
    void RegisterMemoryAllocated(std::string, std::size_t);
    void RegisterMemoryDealocated(std::string);
    void CatchExeptions(std::exception & error);
    int GetOperatorNewCounter();

 //   std::map <std::string, std::string> MemoTypes;

    static MemoryLeakFinder & getMemoryLeakFinder()
    {
        static MemoryLeakFinder Memo;
        return Memo;
    }


private:
    MemoryLeakFinder()=default;
    void LogCriticalError(std::string);
    std::size_t AllocatedMemorySum=0;
    std::size_t DeallocatedMemorySum=0;
    int NewCounter = 0;
    int DeleteCounter = 0;
    std::array <std::string,ErrLogsSize> ErrLogs{};
    std::array<std::pair<std::string,std::size_t>,ArrSize > MemoTypes;
    std::array<std::pair<std::string,int>,ArrSize > MemoNewOrder;

};


#endif // MEMORYLEAKCHECKER_H
