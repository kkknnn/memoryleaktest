#include <iostream>
#include <map>
#include "memoryleakchecker.h"
#include<sstream>
#include <exception>

using namespace std;


class MemoryLeakFinder;

void * operator new(size_t size)
{

    MemoryLeakFinder::getMemoryLeakFinder().AddOperatorNew();
    MemoryLeakFinder::getMemoryLeakFinder().AddAllocatedMemory(size);
    void *ptr = malloc(size);
    std::stringstream ss;
    ss << ptr;
    MemoryLeakFinder::getMemoryLeakFinder().RegisterMemoryAllocated(ss.str(),size);
    return ptr;

}

void operator delete(void *ptr) noexcept
{
MemoryLeakFinder::getMemoryLeakFinder().AddOperatorDelete();
free(ptr);
std::stringstream ss;
ss << ptr;

MemoryLeakFinder::getMemoryLeakFinder().RegisterMemoryDealocated(ss.str());

}


int main()
{try{
    int * temp3 = new int{3};
    double * temp = new double{3.2};
    *temp+=1;
    double * temp2 = new double{3.2};

    *temp3+=1;
    *temp2+=1;
//    int * i = nullptr;
//    delete i;

    delete  temp;
    delete temp3;
//   bad_alloc a;
//    throw (a);

    MemoryLeakFinder::getMemoryLeakFinder().MemoryLeakCheck();

    return 0;
}
    catch(bad_alloc & a)
    {

        MemoryLeakFinder::getMemoryLeakFinder().CatchExeptions(a);
        MemoryLeakFinder::getMemoryLeakFinder().MemoryLeakCheck();
        EXIT_FAILURE;
    }
}
